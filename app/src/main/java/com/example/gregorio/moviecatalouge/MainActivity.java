package com.example.gregorio.moviecatalouge;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_main)
    DrawerLayout drawer;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    ActionBarDrawerToggle toggle;
    public static final String LANG = "en-US";
    public static final String API_KEY = BuildConfig.MOVIE_DB_API_KEY;
    public static final int NOTIFICATION_ID = 123;
    public static final int UPCOME_ID = 122;

    AlarmManager alarmManager;
    AlarmPreference alarmPreference;
    AlarmReceiver alarmReceiver;
    PendingIntent notifyPendingIntent, upcomePendingIntent;
    private NotificationManager notificationManager;
    private SchedullerService schedullerService;
    private SchedullerTask schedullerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        navigationView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            Fragment currentFragment = new HomeFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_content, currentFragment)
                    .commit();
        }


        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent upcomeIntent = new Intent(this, SchedullerService.class);
        Intent notifyIntent = new Intent(this, AlarmReceiver.class);

        upcomePendingIntent = PendingIntent.getBroadcast(
                this,
                UPCOME_ID,
                upcomeIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );


        notifyPendingIntent = PendingIntent.getBroadcast(
                this,
                NOTIFICATION_ID,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        schedullerTask = new SchedullerTask(this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_open, R.string.navigation_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        drawer.removeDrawerListener(toggle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()) {
            case R.id.action_setting:
                Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @SuppressWarnings("StatementKosong")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Bundle bundle = new Bundle();
        Fragment fragment = null;
        String home = getString(R.string.home);
        String title = "";
        String search = getString(R.string.cari);
        String favourite = getString(R.string.favorit);
        String alarm = "Alarm";
        if (id == R.id.nav_home) {
            title = home;
            fragment = new HomeFragment();
        } else if (id == R.id.nav_search) {
            title = search;
            fragment = new SearchingFragment();
            fragment.setArguments(bundle);
        } else if (id == R.id.nav_favorite) {
            title = favourite;
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.gregorio.favouritelist");
            if (launchIntent != null) {
                startActivity(launchIntent);
            }
        } else if (id == R.id.nav_setting2) {
            title = alarm;
            Intent alrIntent = new Intent(MainActivity.this, SetAlarmNotif.class);
            startActivity(alrIntent);
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .commit();
        }
        getSupportActionBar().setTitle(title);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
