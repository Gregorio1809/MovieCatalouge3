package com.example.gregorio.moviecatalouge;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;
import static com.example.gregorio.moviecatalouge.MainActivity.NOTIFICATION_ID;
import static com.example.gregorio.moviecatalouge.MainActivity.UPCOME_ID;

public class AlarmReceiver extends BroadcastReceiver {
        public static final String TYPE_ONE_TIME = "NewsAlarm";
        public static final String TYPE_REPEATING = "UpcomeAlarm";
        public static final String EXTRA_TYPE = "type";
        private final int NOTIF_ID_ONETIME = 123;
        private final int NOTIF_ID_REPEATING = 122;


    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);
        String title = "News Alarm";
        int notifId = 123;
        showNewsNotif(context, title, notifId);
    }

    private void showNewsNotif(Context context, String title, int notifId){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Intent contentIntent = new Intent(context, MainActivity.class);
        Uri uri = Uri.parse(DatabaseContract.CONTENT_URI+"/");
        contentIntent.setData(uri);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(
                context,
                NOTIFICATION_ID,
                contentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_local_movies_black)
                .setContentTitle(title)
                .setContentText("Check our news update about movie")
                .setContentIntent(contentPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        notificationManager.notify(notifId, builder.build());
    }

    public void setRepeatingAlarm(Context context, String type, String time){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, type);
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);
        int requestCode = NOTIF_ID_ONETIME;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(context, "News Alarm is Set Up", Toast.LENGTH_SHORT).show();
    }


}
