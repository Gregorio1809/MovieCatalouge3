package com.example.gregorio.moviecatalouge;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SetAlarmNotif extends AppCompatActivity implements View.OnClickListener{
    private TextView tvTwoTime, tvOneTimeTime ;
    private TextView tvRepeatingTime;
    private EditText edtOneTimeMessage , edtRepeatingMessage;
    private Button btnOneTimeTime, btnOneTime ,btnTwoTime ,btnTwo,  btnCancelRepeatingAlarm;
    private Calendar calTwoTimeTime, calOneTimeTime , calRepeatTimeTime;
    private AlarmReceiver alarmReceiver;
    private SchedullerService schedullerService;
    private AlarmPreference alarmPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm_notif);
        tvOneTimeTime = (TextView)findViewById(R.id.tv_one_time_alarm_time);
        tvTwoTime = (TextView)findViewById(R.id.tv_two_time_alarm_time);
        btnOneTimeTime = (Button)findViewById(R.id.btn_one_time_alarm_time);
        btnTwoTime = (Button)findViewById(R.id.btn_two_time_alarm_time);
        btnOneTime = (Button)findViewById(R.id.btn_set_one_time_alarm);
        btnTwo = (Button)findViewById(R.id.btn_set_two_time_alarm);
        btnOneTimeTime.setOnClickListener(this);
        btnOneTime.setOnClickListener(this);
        btnTwoTime.setOnClickListener(this);
        btnTwo.setOnClickListener(this);
        calOneTimeTime = Calendar.getInstance();
        calTwoTimeTime = Calendar.getInstance();
        calRepeatTimeTime = Calendar.getInstance();
        alarmPreference = new AlarmPreference(this);
        alarmReceiver = new AlarmReceiver();
        schedullerService = new SchedullerService();


    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_one_time_alarm_time){
            final Calendar currentDate = Calendar.getInstance();
            new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    calOneTimeTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calOneTimeTime.set(Calendar.MINUTE, minute);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                    tvOneTimeTime.setText(dateFormat.format(calOneTimeTime.getTime()));
                    //Log.v(TAG, "The choosen one " + date.getTime());
                }
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
        }
        else if (v.getId()==R.id.btn_set_one_time_alarm){
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            String repeatTimeTime = timeFormat.format(calOneTimeTime.getTime());
            alarmPreference.setRepeatingTime(repeatTimeTime);
            alarmReceiver.setRepeatingAlarm(this, AlarmReceiver.TYPE_REPEATING,
                    alarmPreference.getRepeatingTime());
        }
        else if (v.getId() == R.id.btn_two_time_alarm_time){
            final Calendar currentDate = Calendar.getInstance();
            new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    calTwoTimeTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    calTwoTimeTime.set(Calendar.MINUTE, minute);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
                    tvTwoTime.setText(dateFormat.format(calTwoTimeTime.getTime()));
                    //Log.v(TAG, "The choosen one " + date.getTime());
                }
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), true).show();
        }
        else if (v.getId()==R.id.btn_set_two_time_alarm){
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            String repeatTimeTime = timeFormat.format(calTwoTimeTime.getTime());
            alarmPreference.setRepeatingTime(repeatTimeTime);
            schedullerService.setRepeatingAlarmm(this, AlarmReceiver.TYPE_REPEATING,
                    alarmPreference.getRepeatingTime());
        }

    }


}