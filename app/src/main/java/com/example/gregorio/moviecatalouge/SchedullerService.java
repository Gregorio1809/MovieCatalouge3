package com.example.gregorio.moviecatalouge;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.ConnectException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;
import static com.example.gregorio.moviecatalouge.AlarmReceiver.EXTRA_TYPE;
import static com.example.gregorio.moviecatalouge.MainActivity.UPCOME_ID;

public class SchedullerService extends BroadcastReceiver {
    public static final String TYPE_ONE_TIME = "NewsAlarm";
    public static final String TYPE_REPEATING = "UpcomeAlarm";
    public static final String EXTRA_TYPE = "type";
    private final int NOTIF_ID_ONETIME = 123;
    private final int NOTIF_ID_REPEATING = 122;
    public static final String TAG_TASK = "UpcomeTask";
    FilmItems items;
    public static final String TAG = "Upcome";
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);

        String title = "News Alarm";
        int notifId = 123;
        getUpcomingData(context);
    }

    private void getUpcomingData(final Context context) {
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<FilmItems> filmItemses = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/movie/upcoming?api_key=" + BuildConfig.MOVIE_DB_API_KEY + "&language=en-US";

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                setUseSynchronousMode(true);
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                try {
                    String hasil = new String(responseBody);
                    JSONObject responseObject = new JSONObject(hasil);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject film = list.getJSONObject(i);
                        FilmItems filmItems = new FilmItems(film);
                        filmItemses.add(filmItems);
                        if (i < 2) {
                            showNotification(context, filmItems);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

            }
        });


    }

    private void showNotification(Context context, FilmItems items) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Intent contentIntent = new Intent(context, MainActivity.class);
        Uri uri = Uri.parse(DatabaseContract.CONTENT_URI + "/" + items.getJudulFilm());
        contentIntent.setData(uri);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(
                context,
                UPCOME_ID,
                contentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        String tanggal_rilis = items.getTanggal();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(tanggal_rilis);
            SimpleDateFormat new_date = new SimpleDateFormat("dd/MM/yyyy");
            String date_release = new_date.format(date);
            String tanggal = items.setTanggal(date_release);

            NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                    .setContentTitle("New movie is coming!")
                    .setSmallIcon(R.drawable.ic_local_movies_black)
                    .setContentText(items.getJudulFilm() + " on " + tanggal)
                    .setContentIntent(contentPendingIntent)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            notificationManager.notify(UPCOME_ID, notification.build());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    public void setRepeatingAlarmm(Context context, String type, String time) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, SchedullerService.class);
        intent.putExtra(EXTRA_TYPE, type);
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);
        int requestCode = NOTIF_ID_REPEATING;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(context, "Upcome Alarm is Set Up", Toast.LENGTH_SHORT).show();
    }


}
